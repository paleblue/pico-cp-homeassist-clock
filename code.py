import asyncio
import board
import busio
import digitalio
import storage
import os
import gc
import time
import json
import wifi
import socketpool
import audiobusio
import audiomixer
import adafruit_minimqtt.adafruit_minimqtt as MQTT
import adafruit_ds3231
import adafruit_max1704x
import adafruit_ntp
import adafruit_sdcard
from adafruit_debouncer import Debouncer
import audiomp3


# Get wifi details and more from a secrets.py file
try:
    from secrets import secrets
except ImportError:
    print("WiFi secrets are kept in secrets.py, please add them there!")
    raise


# ----------------------
# globals initialization
# ----------------------
setting_can_trigger_audio_with_button = True
setting_alarm_mp3 = 'default.mp3'
setting_alarm_time = ''
setting_alarm_time = ''
setting_alarm_enabled = False
setting_snooze_enabled = False
setting_mp3_files = []
setting_play_alarm = False
setting_dst_enabled = False
rtc: adafruit_ds3231.DS3231 = None
audio: audiobusio.I2SOut = None
mixer: audiomixer.Mixer = None
client: MQTT.MQTT = None
max17: adafruit_max1704x.MAX17048 = None
status_sdcard_available = False
status_battmonitor_available = False
status_dac_available = False
status_rtc_available = False
status_playing_alarm = False
mp3: audiomp3.MP3Decoder = None

mqtt_status_battperc = "stat/jacob/esp32_clock/battery"
mqtt_status_timestamp = "stat/jacob/esp32_clock/time"
mqtt_status_battperc = "stat/jacob/esp32_clock/battery"
mqtt_status_mp3list = "stat/jacob/esp32_clock/alarm/mp3list"
mqtt_status_alarm_enabled = "stat/jacob/esp32_clock/alarm/enabled"
mqtt_status_sdcard_available = "stat/jacob/esp32_clock/status/sdcard"
mqtt_status_battmonitor_available = "stat/jacob/esp32_clock/status/battmonitor"
mqtt_status_dac_available = "stat/jacob/esp32_clock/status/dac"
mqtt_status_rtc_available = "stat/jacob/esp32_clock/status/rtc"
mqtt_status_playing_alarm = "stat/jacob/esp32_clock/status/playing_alarm"
mqtt_status_update = "stat/jacob/esp32_clock/status"
mqtt_cmnd_mp3_select = "cmnd/jacob/esp32_clock/alarm/mp3/set"
mqtt_cmnd_alarm_set = "cmnd/jacob/esp32_clock/alarm/time/set"
mqtt_cmnd_alarm_enable = "cmnd/jacob/esp32_clock/alarm/enabled/set"
mqtt_cmnd_dst_set = "cmnd/jacob/esp32_clock/alarm/dst/set"
btn_alarmsnooze_pin = board.GP7
btn_alarmtoggle_pin = board.GP6
led_alarmstatus_pin = board.GP8


# -----------------
# utility functions
# -----------------
def datetime_str(t: time.struct_time):
    global setting_dst_enabled
    hoffset = secrets['timezone_offset']
    if setting_dst_enabled:
        hoffset += 1

    if hoffset < 0:
        sign = '-'
    return f"{t.tm_year}-{t.tm_mon:02d}-{t.tm_mday:02d}T{t.tm_hour:02d}:{t.tm_min:02d}:{t.tm_sec:02d}{sign}{abs(hoffset):02d}00"


def mp3_friendly_format(m):
    return m.replace('.mp3', '').replace('_', ' ')


def last_day_of_month(m, y):
    if m in [1, 3, 5, 7, 8, 10, 12]:
        return 31
    elif m in [4, 6, 9, 11]:
        return 30
    elif y % 4 == 0 and (y % 100 != 0 or y % 400 == 0):
        return 29
    else:
        return 28


def adjusted_datetime(orig: time.struct_time) -> time.struct_time:
    global setting_dst_enabled
    newhour = orig.tm_hour + secrets['timezone_offset']
    newday = orig.tm_mday
    newmonth = orig.tm_mon
    newyear = orig.tm_year

    newgmtoff = secrets['timezone_offset'] * 60 * 60

    # NOTE the following only works with adjustments of a day or less
    if setting_dst_enabled:
        newhour += 1
        newgmtoff += 60 * 60

    if newhour < 0:
        newhour += 24
        newday -= 1
    elif newhour >= 24:
        newhour -= 24
        newday += 1

    if newday < 1:
        newmonth -= 1
        if newmonth < 1:
            newmonth = 12
            newyear -= 1
        newday = last_day_of_month(newmonth, newyear)
    elif newday > last_day_of_month(newmonth, newyear):
        newday = 1
        newmonth += 1
        if newmonth > 12:
            newmonth = 1
            newyear += 1

    return time.struct_time([
        newyear,
        newmonth,
        newday,
        newhour,
        orig.tm_min,
        orig.tm_sec,
        -1,
        -1,
        -1,
    ])


# --------------
# MQTT functions
# --------------
def connected(client: MQTT.MQTT, userdata, flags, rc):
    global mqtt_cmnd_alarm_enable, mqtt_cmnd_alarm_set, mqtt_cmnd_mp3_select, mqtt_cmnd_dst_set
    # This function will be called when the client is connected
    # successfully to the broker.
    print("Connected to MQTT server!")
    mqtt_send_config(client)
    # print("Listening for topic changes on %s" % onoff_feed)
    # Subscribe to all changes on the onoff_feed.
    client.subscribe(mqtt_cmnd_mp3_select)
    client.subscribe(mqtt_cmnd_alarm_set)
    client.subscribe(mqtt_cmnd_alarm_enable)
    client.subscribe(mqtt_cmnd_dst_set)
    gc.collect()


def disconnected(client: MQTT.MQTT, userdata, rc):
    # This method is called when the client is disconnected
    print("Disconnected from MQTT server")
    gc.collect()


def message(client: MQTT.MQTT, topic, message):
    global mqtt_cmnd_alarm_enable, mqtt_cmnd_alarm_set, mqtt_cmnd_mp3_select, setting_alarm_enabled, setting_snooze_enabled, setting_play_alarm, rtc, setting_mp3_files, status_sdcard_available, mp3, setting_dst_enabled
    gc.collect()
    # print("New message on topic {0}: {1}".format(topic, message))

    if topic == mqtt_cmnd_alarm_enable:
        print("received update to mqtt_cmnd_alarm_enable with message: ", message)
        if message == 'on':
            print("MQTT: change setting_alarm_enabled to True")
            setting_alarm_enabled = True
        else:
            print("MQTT: change setting_alarm_enabled to False")
            setting_alarm_enabled = False
            setting_snooze_enabled = False
            setting_play_alarm = False
    elif topic == mqtt_cmnd_dst_set:
        print("received update to mqtt_cmnd_dst_set with message: ", message)
        if message == 'on':
            print("MQTT: change setting_dst_enabled to True")
            setting_dst_enabled = True
        else:
            print("MQTT: change setting_dst_enabled to False")
            setting_dst_enabled = False

        # get new ntp, don't use global
        ntp: adafruit_ntp.NTP = adafruit_ntp.NTP(pool, tz_offset=0)
        if ntp and rtc:
            rtc.datetime = adjusted_datetime(ntp.datetime)
            print('Set RTC time with NTP time', datetime_str(ntp.datetime))
    elif topic == mqtt_cmnd_mp3_select:
        print("received update to mqtt_cmnd_mp3_select with message: ", message)
        foundit = False
        for m in setting_mp3_files:
            if mp3_friendly_format(m) == message:
                print('updated setting_alarm_mp3', m)
                foundit = True
                setting_alarm_mp3 = m

        if foundit == False:
            print('could not match', message, 'in', setting_mp3_files)
        elif status_sdcard_available:
            print('opening file', f"/sd/{setting_alarm_mp3}")
            mp3.file = open(f"/sd/{setting_alarm_mp3}", "rb")
        else:
            print("can't open file on sdcard, sdcard not available")
    elif topic == mqtt_cmnd_alarm_set:
        [t_hr, t_min, t_sec] = [int(x) for x in message.split(':')]
        t_hr %= 24
        alarm = [time.struct_time([
            rtc.datetime.tm_year,
            rtc.datetime.tm_mon,
            rtc.datetime.tm_mday,
            t_hr,
            t_min,
            t_sec,
            -1,
            -1,
            -1
        ]), 'daily']
        print("received update to mqtt_cmnd_alarm_set",
              message, alarm)
        rtc.alarm1 = alarm
        print("alarm1 is now", rtc.alarm1)
    else:
        print("New unknown message on topic {0}: {1}".format(topic, message))

    gc.collect()


def mqtt_send_config(client: MQTT.MQTT):
    global mqtt_status_update, mqtt_status_timestamp
    o_timestamp = {
        "~": "homeassistant/light/jacob_esp32_clock",
        "device_class": "timestamp",
        "name": "Jacob's Wall Clock Pico Time",
        "state_topic": mqtt_status_timestamp,
        "value_template": "{{ value_json.timestamp }}",
        "unique_id": "je32sclk_wallcontrol2__timestamp",
        "qos": 0,
        "optimistic": False,
        "retain": True,
        "expire_after": 120,
        "device": {
            "identifiers": [
                "je32sclk_wallcontrol2"
            ]
        }
    }
    j_timestamp = json.dumps(o_timestamp)
    print("CONFIG: sending timestamp to mqtt", j_timestamp)
    client.publish(
        'homeassistant/sensor/jacob_esp32_clock/timestamp/config', j_timestamp)
    del o_timestamp
    del j_timestamp

    o_status_sdcard = {
        "~": "homeassistant/binary_sensor/jacob_esp32_clock/status",
        "name": "Jacob's Wall Clock SDCard Availability",
        "state_topic": mqtt_status_update,
        "value_template": "{{ value_json.sdcard }}",
        "device_class": "problem",
        "payload_on": "unavailable",
        "payload_off": "available",
        "unique_id": "je32sclk_wallcontrol2_status_sdcard",
        "qos": 0,
        "optimistic": False,
        "retain": True,
        "expire_after": 360,
        "device": {
            "identifiers": [
                "je32sclk_wallcontrol2"
            ]
        }
    }
    j_status_sdcard = json.dumps(o_status_sdcard)
    print("CONFIG: sending status_sdcard to mqtt", j_status_sdcard)
    client.publish(
        'homeassistant/binary_sensor/jacob_esp32_clock/status_sdcard/config', j_status_sdcard)
    del o_status_sdcard
    del j_status_sdcard

    o_status_battmonitor = {
        "~": "homeassistant/binary_sensor/jacob_esp32_clock/status",
        "name": "Jacob's Wall Clock battmonitor Availability",
        "state_topic": mqtt_status_update,
        "value_template": "{{ value_json.battmonitor }}",
        "device_class": "problem",
        "payload_on": "unavailable",
        "payload_off": "available",
        "unique_id": "je32sclk_wallcontrol2_status_battmonitor",
        "qos": 0,
        "optimistic": False,
        "retain": True,
        "expire_after": 360,
        "device": {
            "identifiers": [
                "je32sclk_wallcontrol2"
            ]
        }
    }
    j_status_battmonitor = json.dumps(o_status_battmonitor)
    print("CONFIG: sending status_battmonitor to mqtt", j_status_battmonitor)
    client.publish(
        'homeassistant/binary_sensor/jacob_esp32_clock/status_battmonitor/config', j_status_battmonitor)
    del o_status_battmonitor
    del j_status_battmonitor

    o_status_dac = {
        "~": "homeassistant/binary_sensor/jacob_esp32_clock/status",
        "name": "Jacob's Wall Clock DAC Availability",
        "state_topic": mqtt_status_update,
        "value_template": "{{ value_json.dac }}",
        "device_class": "problem",
        "payload_on": "unavailable",
        "payload_off": "available",
        "unique_id": "je32sclk_wallcontrol2_status_dac",
        "qos": 0,
        "optimistic": False,
        "retain": True,
        "expire_after": 360,
        "device": {
            "identifiers": [
                "je32sclk_wallcontrol2"
            ]
        }
    }
    j_status_dac = json.dumps(o_status_dac)
    print("CONFIG: sending status_dac to mqtt", j_status_dac)
    client.publish(
        'homeassistant/binary_sensor/jacob_esp32_clock/status_dac/config', j_status_dac)
    del o_status_dac
    del j_status_dac

    o_status_rtc = {
        "~": "homeassistant/binary_sensor/jacob_esp32_clock/status",
        "name": "Jacob's Wall Clock RTC Availability",
        "state_topic": mqtt_status_update,
        "value_template": "{{ value_json.rtc }}",
        "device_class": "problem",
        "payload_on": "unavailable",
        "payload_off": "available",
        "unique_id": "je32sclk_wallcontrol2_status_rtc",
        "qos": 0,
        "optimistic": False,
        "retain": True,
        "expire_after": 360,
        "device": {
            "identifiers": [
                "je32sclk_wallcontrol2"
            ]
        }
    }
    j_status_rtc = json.dumps(o_status_rtc)
    print("CONFIG: sending status_rtc to mqtt", j_status_rtc)
    client.publish(
        'homeassistant/binary_sensor/jacob_esp32_clock/status_rtc/config', j_status_rtc)
    del o_status_rtc
    del j_status_rtc

    o_playing_alarm = {
        "~": "homeassistant/binary_sensor/jacob_esp32_clock/status",
        "name": "Jacob's Wall Clock Alarm is Playing",
        "state_topic": mqtt_status_update,
        "value_template": "{{ value_json.playing_alarm }}",
        "device_class": "sound",
        "payload_on": "playing",
        "payload_off": "not playing",
        "unique_id": "je32sclk_wallcontrol2_playing_alarm",
        "qos": 0,
        "optimistic": False,
        "retain": True,
        "expire_after": 360,
        "device": {
            "identifiers": [
                "je32sclk_wallcontrol2"
            ]
        }
    }
    j_playing_alarm = json.dumps(o_playing_alarm)
    print("CONFIG: sending playing_alarm to mqtt", j_playing_alarm)
    client.publish(
        'homeassistant/binary_sensor/jacob_esp32_clock/playing_alarm/config', j_playing_alarm)
    del o_playing_alarm
    del j_playing_alarm

    gc.collect()


def send_mp3_list():
    global client, mqtt_status_mp3list, setting_mp3_files
    j_mp3list = json.dumps([mp3_friendly_format(x)
                           for x in setting_mp3_files])
    print("CONFIG: sending mp3 list", j_mp3list)
    client.publish(
        mqtt_status_mp3list, j_mp3list)
    gc.collect()


def send_battery_status_update():
    global max17, mqtt_status_battperc
    o_battperc = {
        "percent_remaining": max17.cell_percent,
        "voltage": max17.cell_voltage,
        "charge_rate": max17.charge_rate
    }
    j_battperc = json.dumps(o_battperc)
    # print("STATUS: sending battery percentage to mqtt", j_battperc)
    client.publish(mqtt_status_battperc, j_battperc)
    gc.collect()


def send_time_status_update():
    global rtc, client, mqtt_status_timestamp
    o_time = {
        "timestamp": datetime_str(rtc.datetime),
        "alarm1_enabled": setting_alarm_enabled,
        "alarm1": datetime_str(rtc.alarm1[0]),
        "alarm1_period": rtc.alarm1[1],
        "alarm1_status": rtc.alarm1_status,
        "alarm2_enabled": setting_snooze_enabled,
        "alarm2": datetime_str(rtc.alarm2[0]),
        "alarm2_period": rtc.alarm2[1],
        "alarm2_status": rtc.alarm2_status,
    }
    j_time = json.dumps(o_time)
    # print("STATUS: sending time info to mqtt", j_time)
    client.publish(mqtt_status_timestamp, j_time)

    del o_time
    del j_time
    gc.collect()


def send_alarm_enabled_status():
    global setting_alarm_enabled, client, mqtt_status_alarm_enabled
    print("STATUS: update alarm enabled status", setting_alarm_enabled)
    if setting_alarm_enabled == True:
        client.publish(mqtt_status_alarm_enabled, 'true')
    else:
        client.publish(mqtt_status_alarm_enabled, 'false')

    gc.collect()


def send_status_update():
    global client, mqtt_status_update, status_sdcard_available, status_battmonitor_available, status_dac_available, status_rtc_available, status_playing_alarm
    str_sdcard = 'unavailable'
    if status_sdcard_available:
        str_sdcard = 'available'

    str_battmonitor = 'unavailable'
    if status_battmonitor_available:
        str_battmonitor = 'available'

    str_dac = 'unavailable'
    if status_dac_available:
        str_dac = 'available'

    str_rtc = 'unavailable'
    if status_rtc_available:
        str_rtc = 'available'

    str_playing_alarm = 'not playing'
    if status_playing_alarm:
        str_playing_alarm = 'playing'

    o_update = {
        "sdcard": str_sdcard,
        "battmonitor": str_battmonitor,
        "dac": str_dac,
        "rtc": str_rtc,
        "playing_alarm": str_playing_alarm,
    }
    j_update = json.dumps(o_update)
    client.publish(mqtt_status_update, j_update)
    gc.collect()


# -----------
# async loops
# -----------
async def ping_charger(key_pin, interval_on, interval_off):
    global max17, status_battmonitor_available
    battmon_key = digitalio.DigitalInOut(key_pin)
    battmon_key.direction = digitalio.Direction.OUTPUT
    battmon_key.value = True

    i2c_bat = busio.I2C(scl=board.GP5, sda=board.GP4, frequency=200000)

    print("Battery Monitor (I2C1) scan:")
    while i2c_bat.try_lock():
        pass

    try:
        scan_results_bat = i2c_bat.scan()
        i2c_bat.unlock()
        print([hex(x) for x in scan_results_bat])

        max17 = adafruit_max1704x.MAX17048(i2c_bat)
        print(
            "Found MAX1704x Battery Monitor with chip version",
            hex(max17.chip_version),
            "and id",
            hex(max17.chip_id),
        )
        print(f"Battery voltage: {max17.cell_voltage:.2f} Volts")
        print(f"Battery state  : {max17.cell_percent:.1f} %")

        status_battmonitor_available = True

        send_battery_status_update()
        gc.collect()

        while True:
            battmon_key.value = False
            # print('key_pin to False')
            await asyncio.sleep(interval_on)
            battmon_key.value = True
            # print('key_pin to True')
            send_battery_status_update()
            await asyncio.sleep(interval_off)

    except Exception:
        print("Failed to find charger")
        status_battmonitor_available = False


async def update_timestamp(interval):
    print("running update_timestamp task")
    send_time_status_update()

    while True:
        await asyncio.sleep(interval)
        send_time_status_update()
        gc.collect()


async def mqtt_polling(interval):
    global client
    while True:
        client.loop()
        await asyncio.sleep(interval)
        gc.collect()


async def btn_polling(interval):
    global btn_alarmtoggle_pin, btn_alarmsnooze_pin, led_alarmstatus_pin, setting_alarm_enabled, setting_snooze_enabled, setting_play_alarm
    btn_alarmtoggle = digitalio.DigitalInOut(btn_alarmtoggle_pin)
    btn_alarmtoggle.direction = digitalio.Direction.INPUT
    btn_alarmtoggle.pull = digitalio.Pull.UP

    btn_alarmsnooze = digitalio.DigitalInOut(btn_alarmsnooze_pin)
    btn_alarmsnooze.direction = digitalio.Direction.INPUT
    btn_alarmsnooze.pull = digitalio.Pull.UP

    led_alarmstatus = digitalio.DigitalInOut(led_alarmstatus_pin)
    led_alarmstatus.direction = digitalio.Direction.OUTPUT
    led_alarmstatus.value = setting_alarm_enabled

    switch_alarmtoggle = Debouncer(btn_alarmtoggle)
    switch_alarmtoggle_pressed = False
    switch_alarmsnooze = Debouncer(btn_alarmsnooze)
    switch_alarmsnooze_pressed = False

    while True:
        switch_alarmtoggle.update()
        switch_alarmsnooze.update()

        if switch_alarmtoggle.value == False and switch_alarmtoggle_pressed == False:
            print("switch_alarmtoggle pressed")
            switch_alarmtoggle_pressed = True
            if setting_alarm_enabled:
                setting_alarm_enabled = False
                print("toggling alarm enabled to False")
            else:
                setting_alarm_enabled = True
                print("toggling alarm enabled to True")
            send_alarm_enabled_status()
            gc.collect()
        elif switch_alarmtoggle.value and switch_alarmtoggle_pressed:
            print("switch_alarmtoggle released")
            switch_alarmtoggle_pressed = False

        if led_alarmstatus.value != setting_alarm_enabled:
            led_alarmstatus.value = setting_alarm_enabled

        # ----------

        if switch_alarmsnooze.value == False and switch_alarmsnooze_pressed == False:
            print("switch_alarmsnooze pressed")
            switch_alarmsnooze_pressed = True
            if status_rtc_available:
                if setting_play_alarm:
                    rtc.alarm1_status = False
                    rtc.alarm2_status = False
                    setting_play_alarm = False
                    newhour = rtc.alarm1[0].tm_hour
                    newminute = rtc.alarm1[0].tm_min
                    if setting_snooze_enabled:
                        newhour = rtc.alarm2[0].tm_hour
                        newminute = rtc.alarm2[0].tm_min

                    newminute = newminute + 10
                    if newminute > 59:
                        newminute = newminute - 60
                        newhour = (newhour + 1) % 24

                    next_alarm = [time.struct_time([
                        rtc.datetime.tm_year,
                        rtc.datetime.tm_mon,
                        rtc.datetime.tm_mday,
                        newhour,
                        newminute,
                        0,
                        -1,
                        -1,
                        -1
                    ]), 'daily']
                    rtc.alarm2 = next_alarm
                    setting_snooze_enabled = True
                elif setting_can_trigger_audio_with_button:
                    setting_play_alarm = True
                gc.collect()
        elif switch_alarmsnooze.value and switch_alarmsnooze_pressed:
            print("switch_alarmsnooze released")
            switch_alarmsnooze_pressed = False

        await asyncio.sleep(interval)


async def alarm_polling(interval):
    global status_rtc_available, rtc, setting_play_alarm, status_sdcard_available, status_dac_available, audio, status_playing_alarm
    print('initializing audio')

    while True and status_rtc_available and status_dac_available and status_sdcard_available:
        go_alarm1 = rtc.alarm1_status and setting_alarm_enabled
        go_alarm2 = rtc.alarm2_status and setting_snooze_enabled

        if go_alarm1 or go_alarm2 or setting_play_alarm:
            if (rtc.alarm1_status or rtc.alarm2_status) and setting_play_alarm == False:
                setting_play_alarm = True
            print('alarm1_status', rtc.alarm1_status,
                  setting_play_alarm)
            rtc.alarm1_status = False
            rtc.alarm2_status = False
            # if state.mixer.voice[0].playing:
            #     state.mixer.voice[0].stop()
            if audio.playing:
                audio.stop()

            print('playing', setting_alarm_mp3)

            audio.play(mp3)
            status_playing_alarm = True
            send_status_update()
            while audio.playing:
                print('setting_play_alarm', setting_play_alarm)
                if setting_play_alarm == False:
                    audio.stop()
                await asyncio.sleep(2)

            # state.mixer = audiomixer.Mixer(voice_count=1, channel_count=state.mp3.channel_count,
            #                                sample_rate=state.mp3.sample_rate, bits_per_sample=state.mp3.bits_per_sample)

            # state.mixer.voice[0].level = 0.5
            # state.audio.play(state.mixer)
            # state.mixer.voice[0].play(state.mp3)

            # while state.mixer.voice[0].playing:
            #     print('setting_play_alarm', state.setting_play_alarm)
            #     if state.setting_play_alarm == False:
            #         state.mixer.voice[0].stop()
            #     await asyncio.sleep(2)

            audio.stop()
            setting_play_alarm = False
            status_playing_alarm = False
            send_status_update()
            gc.collect()
        else:
            # if state.mixer.voice[0].playing:
            # state.mixer.voice[0].stop()
            if audio.playing:
                audio.stop()

        await asyncio.sleep(interval)
        gc.collect()

    # failure on either sdcard, rtc, or dac connection
    await asyncio.sleep(interval)
    gc.collect()


async def send_status_update_timer(interval):
    while True:
        send_status_update()
        await asyncio.sleep(interval)


# ---------------------------------------------------------
# create decoder first, it needs the most contiguous memory
# ---------------------------------------------------------
gc.collect()
start_mem = gc.mem_free()
print("Available memory at mp3decoder: {} bytes".format(start_mem))
mp3 = audiomp3.MP3Decoder(open(f"{setting_alarm_mp3}", "rb"))

# ----------------------------------------------------------
# I2C and SPI initialization (RTC and SD card, respectively)
# ----------------------------------------------------------
i2c_rtc = busio.I2C(scl=board.GP3, sda=board.GP2, frequency=200000)
spi = busio.SPI(clock=board.GP18, MOSI=board.GP19, MISO=board.GP16)
sdcard_chipselect = digitalio.DigitalInOut(board.GP17)
gc.collect()

# -------------
# mount SD card
# -------------
presd_mem = gc.mem_free()
print("Available memory (pre-sd): {} bytes".format(presd_mem))
try:
    sdcard = adafruit_sdcard.SDCard(spi, sdcard_chipselect)
    vfs = storage.VfsFat(sdcard)
    storage.mount(vfs, "/sd")

    dir_contents = os.listdir("/sd")
    setting_mp3_files = [
        x for x in dir_contents if x.endswith('.mp3')]
    print('setting_mp3_files', setting_mp3_files)
    status_sdcard_available = True
except Exception as e:
    status_sdcard_available = False
    print('sd mount failed', e)
gc.collect()

# --------------
# connect to DAC
# --------------
try:
    audio = audiobusio.I2SOut(
        bit_clock=board.GP10, word_select=board.GP11, data=board.GP12, left_justified=False)
    status_dac_available = True
except:
    status_dac_available = False
# mixer = audiomixer.Mixer(voice_count=1)
# audio.play(state.mixer)
# if status_sdcard_available:
#     mp3.file = open(f"/sd/{setting_alarm_mp3}", "rb")
gc.collect()

# ---------------
# initialize wifi
# ---------------
wifi.radio.connect(secrets["ssid"], secrets["password"])
pool = socketpool.SocketPool(wifi.radio)

# -------------------------------------
# show addresses on I2C, connect to RTC
# -------------------------------------
try:
    print("RTC (I2C0) scan:")
    while i2c_rtc.try_lock():
        pass

    scan_results = i2c_rtc.scan()
    i2c_rtc.unlock()
    print([hex(x) for x in scan_results])

    rtc = adafruit_ds3231.DS3231(i2c_rtc)
    print('Found RTC with datetime', datetime_str(rtc.datetime))
    status_rtc_available = True
except:
    status_rtc_available = False
gc.collect()

# ----------------------
# connect to time server
# ----------------------
try:
    ntp: adafruit_ntp.NTP = adafruit_ntp.NTP(pool, tz_offset=0)
    if ntp:
        rtc.datetime = adjusted_datetime(ntp.datetime)
        print('Set RTC time with NTP time', datetime_str(ntp.datetime))
        # print('DT', datetime_str(dt))
    else:
        print("couldn't access ntp server")
except:
    print("failed to connect to ntp server")
gc.collect()

# ----------------------
# connect to MQTT server
# ----------------------
premqtt_mem = gc.mem_free()
print("Available memory - pre-mqtt: {} bytes".format(premqtt_mem))
client = MQTT.MQTT(
    broker=secrets['mqtt_server'],
    port=secrets['mqtt_port'],
    username=secrets['mqtt_user'],
    password=secrets['mqtt_pass'],
    socket_pool=pool,
)
client._lw_topic = "jacob/picoclock/lwt"
client._lw_msg = "ESP32 Clock Pico has gone offline"
client._lw_retain = False
client._lw_qos = 1
client.on_connect = connected
client.on_disconnect = disconnected
client.on_message = message
gc.collect()

print('Connecting to MQTT server')
client.connect()
postmqtt_mem = gc.mem_free()
print("Available memory - post-mqtt: {} bytes".format(postmqtt_mem))


async def main():
    # ----------------------
    # send MQTT basic status
    # ----------------------
    send_mp3_list()
    send_alarm_enabled_status()
    send_status_update()

    # ------------------
    # create async tasks
    # ------------------
    ping_charger_task = asyncio.create_task(
        ping_charger(board.GP21, 10, 0.05))
    print("started ping_charger_task")

    send_time_task = asyncio.create_task(
        update_timestamp(15)
    )
    print("started send_time_task")

    mqtt_poll_task = asyncio.create_task(
        mqtt_polling(1)
    )
    print("started mqtt poll task")

    btn_poll_task = asyncio.create_task(
        btn_polling(0.02)
    )
    print("started btn poll task")

    alarm_poll_task = asyncio.create_task(
        alarm_polling(0.02)
    )
    print("started alarm poll task")

    status_update_task = asyncio.create_task(
        send_status_update_timer(5)
    )

    await asyncio.gather(ping_charger_task, send_time_task, mqtt_poll_task, btn_poll_task, alarm_poll_task, status_update_task)

asyncio.run(main())

$circuitPyDriveLetter = $(Get-Volume -FileSystemLabel 'CIRCUITPY').DriveLetter;
$circuitPyDrivePath = "${circuitPyDriveLetter}:/";
echo $circuitPyDrivePath
copy *.py $circuitPyDrivePath
copy *.mpy $circuitPyDrivePath
copy *.mp3 $circuitPyDrivePath
# copy -Recurse lib $circuitPyDrivePath